<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExamController extends Controller
{

    public function __construct()
    {
    }

    public function binaryGap($number)
    {
        $longest_gap = 0;
        $binary = decbin($number); // Convert to binary representation
        $binary = trim($binary, "0"); // Removing starting and ending zero
        $array_of_zero = explode('1', $binary); // Array list of zero

        foreach($array_of_zero as $value)
        {
            if(strlen($value) > $longest_gap) 
            {
                $longest_gap = strlen($value);
            }
        }
        
        return 'Longest binary gap: ' . $longest_gap;
    }

    public function fixMissingElement(Request $request)
    {
        $array = $request->all();

        if(!is_array($array['array'])) {
            $array_list = explode(',', $array['array']);
        } else {
            $array_list = $array['array'];
        }

        sort($array_list);
        $length = count($array_list);

        $array_start = true;
        $new_array_list = array();
        $current_number = null;
        $has_gap = false;

        if($length == 1 && $array_list[$length-1] == '') {
            return 'Empty array';
        }
        
        for ($count = 0; $count < $length; $count++)
        {
            if($array_start) {
                array_push($new_array_list, $array_list[$count]);
                $current_number = $array_list[$count] + 1;
                $array_start = false;
                continue;
            }

            if($array_list[$count] == $current_number) {
                array_push($new_array_list, $array_list[$count]);
                $current_number = $array_list[$count] + 1;
            } else {
                $has_gap = true;
                for($current_number; $current_number <= $array_list[$count]; $current_number++) {
                    array_push($new_array_list, ''.$current_number.'');
                    continue;
                }
            }
        }

        if(!$has_gap) 
        {
            array_unshift($new_array_list, ''.($new_array_list[0] - 1).'');
        }
        
        $return['message'] = 'Fixed missing element';
        $return['array'] = $new_array_list;

        return $return;
    }

    public function unpairedElement(Request $request)
    {
        $array = $request->all();
        $array_list = explode(',', $array['array']);

        $length = count($array_list);
        $count = 0;
        $unpaired_element = 'Unpaired element: ';

        while($count < $length)
        {
            $index = $count + 2;
            if($index < $length) {

                if($array_list[$count] == $array_list[$index])
                {
                    echo 'Equal: '. $array_list[$count] .'=='. $array_list[$index];
                    
                } else {
                    $unpaired_element.= ',' . $array_list[$count];
                }   
                
            }

            $count+=1;
        }

        return $unpaired_element;

    }

    public function findCharacter($string)
    {
        $length = strlen($string); // Length of string
        $array  = array();
        $return = array();
        
        for ($count = 0; $count < $length; $count++) 
        {
            $character = substr($string, $count, 1);
            $array[$count] =  $character;
        }

        $count_values = array_count_values($array); // Combine and count values in array

        // Get most times
        $max_value = max($count_values);
        $max_keys = array_keys($count_values, $max_value);
        rsort($max_keys); // Order by desc
       

        // Get least times
        $min_value = min($count_values);
        $min_keys = array_keys($count_values, $min_value);
        rsort($min_keys); // Order by desc

        $return['most_times'] = $max_keys[0] . ' occurs ' . $max_value . ' times.';
        $return['least_times'] = $min_keys[0] . ' occurs ' . $min_value . ' time.';

        return $return;
    }
}
