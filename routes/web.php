<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Answer 1
$router->get('binary-gap/{number}', [
    'uses' => 'ExamController@binaryGap',
    'name' => 'Binary Gap'
]);

// Answer 2
$router->post('fix-missing-element', [
    'uses' => 'ExamController@fixMissingElement',
    'name' => 'Zero Index'
]);

// Answer 3
$router->post('unpaired-element', [
    'uses' => 'ExamController@unpairedElement',
    'name' => 'Unpaired Element'
]);

// Answer 4
$router->get('find-character/{string}', [
    'uses' => 'ExamController@findCharacter',
    'name' => 'Find Character'
]);